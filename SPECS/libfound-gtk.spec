%global gdk_pixbuf2_version               2.36.5
%global gtk3_version                      3.3.6
%global gtk4_version                      4.4.0
%global glib2_version                     2.53.0
%global po_package                        found-gtk

Name:    libfound-gtk3
Version: 4.0.0
Release: 1%{?dist}
Summary: Utility library for loading - runtime files

License: GPLv2+ and LGPLv2+
URL:     http://www.gnome.org
Source0: %{name}-%{version}.tar.gz

BuildRequires: gcc
BuildRequires: gettext
BuildRequires: gtk-doc
BuildRequires: itstool
BuildRequires: meson
BuildRequires: gdk-pixbuf2-devel >= %{gdk_pixbuf2_version}
BuildRequires: pkgconfig(glib-2.0) >= %{glib2_version}
BuildRequires: pkgconfig(gtk+-3.0) >= %{gtk3_version}
Requires: libfound-gtk-data = %{version}-%{release}

%description
As a library for gtk extension.

%package devel
Summary: As a library for gtk3 extension - development files
License: LGPLv2+
Requires: %{name}%{?_isa} = %{version}-%{release}
Requires: gdk-pixbuf2-devel >= %{gdk_pixbuf2_version}

%description devel
This package provides the include files and static library for the library functions.

%package -n libfound-gtk-data
Summary: Data for %{name}
License: LGPLv2+
BuildArch: noarch

%description -n libfound-gtk-data
Common files for libfound gtk widgets

%prep
%autosetup -p1

%build
%meson -Dgtk_doc=false
%meson_build

%install
%define _unpackaged_files_terminate_build 0
%meson_install

%find_lang %{po_package} --all-name --with-gnome

%files
%doc AUTHORS NEWS README.md
%license COPYING COPYING.LIB
%{_libdir}/libfound-gtk3.so.*

%files devel
%{_includedir}/gtk-3.0/found/*.h
%{_libdir}/pkgconfig/libfound-gtk3.pc
%{_libdir}/libfound-gtk3.so

%files -n libfound-gtk-data -f %{po_package}.lang
%{_datadir}/found-gtk/found-gtk-version.xml

%changelog
* Wed Aug 09 2023 qiangy <qiangy@nfschina.com> 4.0.0-1
- Init package.

